<div id="menubar" class="menubar-inverse ">
    <div class="menubar-fixed-panel">
        <div>
            <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="expanded">
            <a href="#">
                <span class="text-lg text-bold text-primary ">MATERIAL&nbsp;ADMIN</span>
            </a>
        </div>
    </div>
    <div class="menubar-scroll-panel">

        <!-- BEGIN MAIN MENU -->
        <ul id="main-menu" class="gui-controls">

            <!-- BEGIN DASHBOARD -->
            <li>
                <a href="<?php echo base_url('admin/home'); ?>" class="<?php echo getActiveMenu('home'); ?>">
                    <div class="gui-icon"><i class="md md-home"></i></div>
                    <span class="title">Dashboard </span>
                </a>
            </li><!--end /menu-li -->
            <!-- END DASHBOARD -->

            <!-- BEGIN EMAIL -->
            <li class="gui-folder">
                <a href="javascript:void(0);" class="<?php echo getActiveMenu('users'); ?>">
                    <div class="gui-icon"><i class="fa fa-users"></i></div>
                    <span class="title">Users</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="<?php echo base_url('admin/users'); ?>" ><span class="title">User List</span></a></li>
                    <li><a href="<?php echo base_url('admin/users/create'); ?>" ><span class="title">Add New User</span></a></li>
                </ul><!--end /submenu -->
            </li><!--end /menu-li -->

			<li class="gui-folder">
                <a href="javascript:void(0);" class="<?php echo getActiveMenu('Activity'); ?>">
                    <div class="gui-icon"><i class="fa fa-file-text-o"></i></div>
                    <span class="title">Activity</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="<?php echo base_url('admin/activity'); ?>" ><span class="title">Activity List</span></a></li>
                    <li><a href="<?php echo base_url('admin/activity/create'); ?>" ><span class="title">Add New Activity</span></a></li>
                </ul><!--end /submenu -->
            </li><!--end /menu-li -->
            <!-- END LEVELS -->

        </ul><!--end .main-menu -->
        <!-- END MAIN MENU -->

      
    </div><!--end .menubar-scroll-panel-->
</div><!--end #menubar-->