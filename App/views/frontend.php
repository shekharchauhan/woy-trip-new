<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title><?php
                if (isset($meta_title)) {
                    echo $meta_title;
                } else {
                    echo site_title();
                }
                ?></title>
            <meta name="description" content="<?php
            if (isset($meta_description)) {
                echo $meta_description;
            }
            ?>">
                <meta name="keywords" content="<?php
                if (isset($meta_keyword)) {
                    echo $meta_keyword;
                }
                ?>">
                    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/front/style.css"/>
                    <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,300,600,700,800' rel='stylesheet' type='text/css'>
                        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300,200' rel='stylesheet' type='text/css'>
                            <link href="<?php echo base_url(); ?>/assets/front/css/font-awesome.min.css" rel="stylesheet" media="screen">
                                <link href="<?php echo base_url(); ?>/assets/front/css/responsive.css" rel="stylesheet" media="screen" type="text/css"/>
                                <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/front/sidr/stylesheets/jquery.sidr.dark.css">
                                    <script src="<?php echo base_url(); ?>/assets/front/js/jquery.min.js"></script>
                                    <script src="<?php echo base_url(); ?>/assets/front/sidr/jquery.sidr.min.js"></script>
                                    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/front/js/smoothscroll.js"></script>

                                    </head>

                                    <body>
                                        <div class="header">
                                            <div class="container">
                                                <div class="logo-menu">
                                                    <div class="logo">
                                                        <h1><a class="navbar-brand" href="<?php echo base_url('admin/home'); ?>"><?php echo site_title(); ?></a></h1>
                                                    </div>
                                                    <!--<a id="simple-menu" href="#sidr">Toggle menu</a>-->
                                                    <div id="mobile-header">
                                                        <a class="responsive-menu-button" href="#"><img src="<?php echo base_url(); ?>/assets/front/images/11.png"/></a>
                                                    </div>
                                                    <div class="menu" id="navigation">
                                                        <ul>
                                                            <?php
                                                            if ($this->session->userdata('logged_in')) {
                                                                ?>
                                                                <li><a href="<?php echo base_url('admin/home'); ?>">Dashboard</a></li>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <li><a href="<?php echo base_url('admin/users/signin'); ?>">Signin</a></li>
                                                                <?php
                                                            }
                                                            ?>

                                                            <?php /* foreach (getPages('footer') as $pages) { ?>
                                                              <li><a href="<?php echo base_url($pages->alias); ?>.html"><?php echo $pages->title; ?></a></li>
                                                              <?php } */ ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="banner">
                                            <div class="container">
                                                <div class="header-text">
                                                    <p class="big-text">Let's Start The Web Journey</p>
                                                    <h2>Admin Dashboard Panel</h2>
                                                    <div class="button-section">
                                                        <ul>
                                                            <li><a href="<?php echo base_url('admin/home'); ?>" class="top-button green">Admin Login</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="color-border">
                                        </div>

                                        <div class="desc">
                                            <div class="container">
                                                <h2>Lorem ipsum dolor sit amet</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lacinia mi nulla, in imperdiet arcu hendrerit in. Curabitur ut augue facilisis leo malesuada consequat.</p>
                                            </div>
                                        </div>

                                        <div class="features" id="features">
                                            <div class="container">
                                                <h3 class="text-head">Features Of Extant</h3>
                                                <div class="features-section">
                                                    <ul>
                                                        <li>
                                                            <div class="feature-icon icon1"></div>
                                                            <h4>Creative Design</h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lacinia mi nulla, in imperdiet arcu hendrerit in.</p>
                                                        </li>
                                                        <li>
                                                            <div class="feature-icon icon2"></div>
                                                            <h4>Save Your Time</h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lacinia mi nulla, in imperdiet arcu hendrerit in.</p>
                                                        </li>
                                                        <li>
                                                            <div class="feature-icon icon3"></div>
                                                            <h4>Fully Customization</h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lacinia mi nulla, in imperdiet arcu hendrerit in.</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="color-border">
                                        </div>
                                        <div class="footer">
                                            <div class="container">
                                                <div class="infooter">
                                                    <p class="copyright">Developed by <a class="credit"href="http://orangemantra.com/">Orange Mantra</a></p>
                                                </div>
                                                <ul class="socialmedia">
                                                    <li><a href=""><i class="icon-twitter"></i></a></li>
                                                    <li><a href=""><i class="icon-facebook"></i></a></li>
                                                    <li><a href=""><i class="icon-dribbble"></i></a></li>
                                                    <li><a href=""><i class="icon-linkedin"></i></a></li>
                                                    <li><a href=""><i class="icon-instagram"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/front/js/jquery.nicescroll.min.js"></script>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $('#simple-menu').sidr({
                                                    side: 'right'
                                                });
                                            });
                                            $('.responsive-menu-button').sidr({
                                                name: 'sidr-main',
                                                source: '#navigation',
                                                side: 'right'

                                            });
                                            $(document).ready(
                                                    function () {
                                                        $("html").niceScroll({cursorborder: "0px solid #fff", cursorwidth: "5px", scrollspeed: "70"});
                                                    }
                                            );
                                        </script>
                                    </body>
                                    </html>
