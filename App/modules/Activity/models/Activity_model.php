<?php

/*

 */

class Activity_model extends CI_Model {

    public $wo_activities = 'wo_activities';
    public $wo_users = 'wo_users';
    public $media_gallery = 'media_gallery';
    public $table_static_block = 'static_blocks';
    var $pages_table = "pages";
    var $rule_table = "nc_rules";

    function __construct() {
        parent::__construct();
    }

    public function saveBlocks() {
        $action = $this->input->get('S');
        $alias = $this->input->post('section');

        $data['content'] = "";
        switch ($action) {
            case 'f-cms':
                $pids = $this->input->post('flinks');
                if (!empty($pids))
                    $data['content'] = serialize(explode(',', $pids));
                break;
            case 'logo':
                $this->deletelogo();
                $logo_info = $this->uploadLogo();
                if ($logo_info['file_name']) {
                    $data['content'] = $logo_info['file_name'];
                }
                break;

            default:
                $data['content'] = serialize($this->input->post());
                break;
        }
        $this->db->where('alias', $alias);

        $return = $this->db->update($this->wo_activities, $data);
        return $return;
    }

    public function getStaicBlocks($alias = "") {
        if ($alias != "")
            $this->db->where('alias', $alias);

        $query = $this->db->get($this->wo_activities);
        $qqq = $query->result();
        $ret = array();
        foreach ($qqq as $row) {
            $ret[$row->alias] = $row;
        }
        return $ret;
    }

    function uploadLogo() {
        $config = array();
        $config['upload_path'] = IMAGESPATH . 'logo/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->helper(array('form', 'url'));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('logo')) {
            $info = $this->upload->data();
        } else {
            $info = array('file_name' => '');
        }
        return $info;
    }

    function deletelogo() {
        if ($this->input->post('deleteLogo')) {
            $logo = $this->getStaicBlocks();
            $file_name = $logo['logo']->content;
            unlink(IMAGESPATH . 'logo/' . $file_name);
        }
    }

    function getList($showPerpage, $offset) {
        $conditions = array(
            'is_deleted' => '0'
        );
        //$this->db->where($conditions);
        //$this->db->limit($showPerpage, $offset);
		$this->db->order_by('activity_id','DESC');
        $this->db->from($this->wo_activities);
        $result = $this->db->get();
        return $result->result();
    }
	function getListByTable($table){
		/* if($table=='wo_trips'){
			$this->db->where('trip_status', 'ongoing');
		} */
		$this->db->order_by('user_id','DESC');
		$this->db->from($table);
		$result = $this->db->get();
        return $result->result();
	}
	function getResultByAjax($id,$table){
		if($table=='wo_trips'){
			$this->db->where('user_id', $id);
		}
		else if($table=='wo_groups'){
			$this->db->where('user_id', $id);
		}
		$this->db->from($table);
		$result = $this->db->get();
        return $result->result();
	}
    function updateStatus() {
       if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('activity_id', $arr_ids);
                $this->db->delete($this->wo_activities);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $is_active = "0";
            } else if ($this->input->post('Activate') != null) {
                $is_active = "1";
            }
            $data = array(
                'is_active' => $is_active
            );

            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('activity_id', $arr_ids);
                $this->db->update($this->wo_activities, $data);
            }
        }
    }

    function count_all() {
        $conditions = array(
            'is_deleted' => '0'
        );
        $this->db->where($conditions);
        $query = $this->db->get($this->pages_table);
        return $query->num_rows();
    }

    function delete($id) {
        $data = array(
            'is_deleted' => '1',
        );
        $this->db->where('activity_id', $id);
        $query = $this->db->update($this->wo_activities, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function create() {
		$insert_arr = array();
        $data = $this->input->post();
		$data['activity_type'] = 'wall';
		$data['resource_type'] = 'wall';
		/* $dates = explode('-',$data['start_end_date']);
		$data['start_date'] = date('Y-m-d',strtotime($dates[0]));
		$data['end_date'] = date('Y-m-d',strtotime($dates[1]));
		unset($data['start_end_date']); */
		$geo = file_get_contents ( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode ( $data['destination'] ) . '&key=' . GoogleAPIKey );
		$geo = json_decode ( $geo, true ); // print_r($geo); die();
		if ($geo ['status'] = 'OK') {
			$latitude = $geo ['results'] [0] ['geometry'] ['location'] ['lat'];
			$longitude = $geo ['results'] [0] ['geometry'] ['location'] ['lng'];
		} else {
			$latitude = '';
			$longitude = '';
		}
		$data['latitude'] = $latitude;
		$data['longitude'] = $longitude;
		$data['group_id'] = implode(',',$data['group_id']);
		$data1['images'] = $data['image'];
		$data1['location'] = $data['location'];
		unset($data['image']);
		unset($data['location']);
		$query = $this->db->insert($this->wo_activities, $data);
		$activity_id = $this->db->insert_id();
		for($i=0;$i<count($data1['images']);$i++){
			$insert_arr[] = array(
							'media_src'=>$data1['images'][$i],
							'location'=>$data1['location'][$i],
							'media_type'=>'wall',
							'activity_id'=>$activity_id,
							'user_id'=> $data['user_id'],
							'creation_date' => date('Y-m-d h:i:s')
							);
		}
		if(!empty($insert_arr)){
			$count = count($insert_arr);
			$this->db->insert_batch($this->media_gallery,$insert_arr);
			/* $first_id = $this->db->insert_id();
			$last_id = $first_id + ($count-1);
			$media_range = range($first_id,$last_id);
			$media_range = implode(',',$media_range);
			$this->db->where('activity_id',$activity_id);
			$this->db->update($this->wo_activities, array('media_id'=>$media_range)); */
		}
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function update($id) {
        $data = $this->input->post();
		$data['activity_type'] = 'wall';
		$data['resource_type'] = 'wall';
		/* $dates = explode('-',$data['start_end_date']);
		$data['start_date'] = date('Y-m-d',strtotime($dates[0]));
		$data['end_date'] = date('Y-m-d',strtotime($dates[1]));
		unset($data['start_end_date']); */
		$geo = file_get_contents ( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode ( $data['destination'] ) . '&key=' . GoogleAPIKey );
		$geo = json_decode ( $geo, true ); // print_r($geo); die();
		if ($geo ['status'] = 'OK') {
			$latitude = $geo ['results'] [0] ['geometry'] ['location'] ['lat'];
			$longitude = $geo ['results'] [0] ['geometry'] ['location'] ['lng'];
		} else {
			$latitude = '';
			$longitude = '';
		}
		$data['latitude'] = $latitude;
		$data['longitude'] = $longitude;
		$data['group_id'] = implode(',',$data['group_id']);
		$data1['images'] = $data['image'];
		$data1['old_images'] = $data['image1'];
		$data1['images'] = $data['image'];
		$data1['old_location'] = $data['location1'];
		$data1['location'] = $data['location'];
		$data1['image_delete'] = $data['image_delete'];
		unset($data['image']);
		unset($data['image1']);
		unset($data['location1']);
		unset($data['location']);
		unset($data['image_delete']);
        $this->db->where('activity_id', $id);
        $query = $this->db->update($this->wo_activities, $data);
		/* echo '<pre>IMAGE DELETE: ',print_r($data1['image_delete']);
		echo '<pre>OLD IMAGE: ',print_r($data1['old_images']);
		echo '<pre>OLD LOCATION: ',print_r($data1['old_location']);
		echo '<pre>NEW IMAGE: ',print_r($data1['images']);
		echo '<pre>NEW LOCATION: ',print_r($data1['location']); */
		if(count($data1['old_images'])){
			for($i=0;$i<count($data1['old_images']);$i++){
				if(in_array($data1['old_images'][$i],$data1['image_delete'])){
					$img_arr = get_field_value($this->media_gallery,'media_src','id',$data1['old_images'][$i]);
					@unlink(IMAGESPATH . 'activities/'.$img_arr->row()->media_src);
					$this->db->delete($this->media_gallery,array('id'=>$data1['old_images'][$i]));
				}
				else{
					$img_data['location'] = $data1['old_location'][$i];
					$this->db->where('id', $data1['old_images'][$i]);
					$this->db->update($this->media_gallery, $img_data);
				}
			}
		}
		for($i=0;$i<count($data1['images']);$i++){
			$insert_arr[] = array(
							'media_src'=>$data1['images'][$i],
							'location'=>$data1['location'][$i],
							'media_type'=>'wall',
							'activity_id'=>$id,
							'user_id'=> $data['user_id'],
							'creation_date' => date('Y-m-d h:i:s')
							);
		}
		if(!empty($insert_arr)){
			$count = count($insert_arr);
			$this->db->insert_batch($this->media_gallery,$insert_arr);
		}
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function get($id) {
        $this->db->where('activity_id', $id);
        $this->db->from($this->wo_activities);
        $result = $this->db->get();
        return $result->row();
    }

    function getAllParentPage($exclue = 0) {
        if ($exclue != 0)
            $this->db->where('id !=', $exclue);
        $this->db->where('parent_id', '0');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->pages_table);
        return $query->result();
    }

    function getContent($alias) {
        $this->db->where('alias', $alias);
        $query = $this->db->get($this->pages_table);
        return $query->row();
    }

    public function getRules() {
        $query = $this->db->get($this->rule_table);
        return $query->result();
    }

    function getRule($id) {
        $this->db->where('rule_id', $id);
        $this->db->from($this->rule_table);
        $result = $this->db->get();
        return $result->row();
    }

    function updateRule($id) {
        $data = array(
            'description' => $this->input->post('description')
        );
        $this->db->where('rule_id', $id);
        $query = $this->db->update($this->rule_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

}

?>