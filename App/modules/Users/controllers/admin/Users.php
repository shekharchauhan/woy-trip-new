<?php

/*

 */

class Users extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function index() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('admin'));
        }
        if ($this->input->post() != null) {
            $this->user_model->updateStatus();
        }
        $config = array();

        /*$config ['total_rows'] = $arr->num_rows ();
            $config ['per_page'] = 10; // data showing perpage
            $config ["uri_segment"] = 3;
            $choice = $config ["total_rows"] / $config ["per_page"];
            $var = $config ['per_page'];
            // print_r($count); die();
            $config ["num_links"] = round ( $choice );
            $this->pagination->initialize ( $config );
            $page = ($this->uri->segment ( 3 )) ? $this->uri->segment ( 3 ) : 0;
            $data ['image'] = $this->Admin_service->getImageGallery ( $var, $page );*/


        $config['total_rows'] = $this->user_model->count_all();
        /*print_r($config['total_rows']);
        die('asd');*/
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["users"] = $this->user_model->read($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $includeJs = array(
            'assets/js/libs/jquery/jquery-1.11.2.min.js',
            'assets/js/libs/DataTables/jquery.dataTables.min.js',
            'assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js',
            'assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js',
            'assets/js/libs/nanoscroller/jquery.nanoscroller.min.js',
            'assets/js/core/source/App.js',
            'assets/js/core/demo/DemoTableDynamic.js',
        );
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['main_content'] = 'users';
        $this->setData($data);
    }

    function account($id = null) {
		$this->_member_area();
        if ($id != null) {
            $userID = $id;
        } else {
            $userID = $this->session->userdata('userid');
        }
        $includeJs = array(
            'assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
            'assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js',
            'assets/js/core/demo/DemoFormComponents.js'
        );
        $includeCss = array('assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
		
        $data['user'] = $this->user_model->user_by_id($userID);
        if ($_POST) {
            if ($this->input->post('deleteProfileimage')) {
                $this->user_model->deleteexistingProfileImage($data['user']->user_pic, $userID);
            }
            $userdata = new stdClass();
            $userdata->name = $this->input->post('name');
            $userdata->user_mobile = $this->input->post('user_mobile');
            $userdata->user_email = $this->input->post('user_email');
            $userdata->country_code = $this->input->post('country_code');
            $userdata->user_mobile = $this->input->post('user_mobile');
            $userdata->user_address = $this->input->post('user_address');
            $userdata->is_active = $this->input->post('is_active');
            if ($this->input->post('user_password') != '')
            $userdata->user_password = md5($this->input->post('user_password'));
            $insert = $this->user_model->update($userID, $userdata);
            if ($insert) {
                $data['message'] = "Updated succesfully";
                $this->session->set_flashdata('message', 'Updated succesfully ');
                $this->setData($data);
            }
            return;
        }
        $this->setData($data);
    }

    function details($nicename) {
        $data["user"] = $this->user_model->user_by_ids($nicename);
        if ($data["user"]) {

            $data['main_content'] = 'user';
            $this->setData($data);
        } else {
            show_404();
        }
    }

    function signin() {
        //Redirect
        if ($this->_is_logged_in()) {
            redirect('');
        }

        if ($_POST) {
            //Data
            $user_email = $this->input->post('user_email', true);
            $password = $this->input->post('password', true);
            $userdata = $this->user_model->validate($user_email, md5($password));

            //Validation
            if ($userdata) {
                //echo $userdata->status;die;
                if ($userdata->status == 0) {
                    $data['error'] = "Not validated!";
                    $data['main_content'] = 'signin';
                    // $this->setData($data);
                    $this->setData($data);
                } else {

                    if ($this->userCheck($userdata->id)) {
                        $data['userid'] = $userdata->id;
                        $data['logged_in'] = true;
                        $this->session->set_userdata($data);
                        redirect(base_url('admin/home'));
                    } else {
                        $data['error'] = "Not validated!";
                        $data['main_content'] = 'signin';
                        $this->setData($data);
                    }
                }
            } else {
                $data['error'] = "Invalid Credentails!";
                $this->setData($data);
            }

            return;
        }
        $data['main_content'] = 'signin';
        $this->setData($data);
    }

    function is_admin($id) {
        if (@$this->userCheck($id)->role === 1) {
            return true;
        } else {
            return false;
        }
    }

    function userCheck($id) {
        return $this->user_model->user_by_ids($id);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url('admin/home'));
    }

//Hidden Methods not allowed by url request

    function _member_area() {
        if (!$this->_is_logged_in()) {
            redirect('signin');
        }
    }

    function _is_logged_in() {
        if ($this->session->userdata('logged_in')) {
            return true;
        } else {
            return false;
        }
    }

    function userdata() {
        if ($this->_is_logged_in()) {
            return $this->user_model->user_by_id($this->session->userdata('userid'));
        } else {
            return false;
        }
    }

    function _is_admin() {
        if (@$this->userdata()->role === 1) {
            return true;
        } else {
            return false;
        }
    }

    function forgot_password() {
        $data['meta_title'] = "Forgot Password";
        $data['meta_keyword'] = "Forgot Password";
        $data['meta_description'] = "Forgot Password";
        $data['main_content'] = 'forgot_password';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $this->load->library('email');
            $key = $this->user_model->getUserKey($this->input->post('user_email'));
            $message = 'Please follow the link to reset your password:' . base_url() . 'admin/users/reset_password?key=' . $key;
            $this->email->from(ADMIN_EMAIL, 'Admin');
            $this->email->to($this->input->post('user_email'));
            $this->email->subject('Login Password Reset');
            $this->email->message($message);
            $this->email->send();
            $this->session->set_flashdata('message', 'Mail Sent Successfully! Please check ');
        }
        $this->setData($data);
    }

    function reset_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
        $this->form_validation->set_rules('cnf_password', 'Confirm Password', 'trim|required|matches[new_password]');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $key = $this->input->get('key');
            $update = $this->user_model->reset_password($key);
            if ($update) {
                redirect('admin/users/signin');
            } else {
                $data['error'] = 'Not Updated';
            }
        }
        $this->setData($data);
    }

    function create() {
        //check_auth();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array(
            'assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
            'assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js',
            'assets/js/core/demo/DemoFormComponents.js'
        );
        $includeCss = array('assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
         $this->form_validation->set_rules('user_mobile', 'Mobile', 'required|is_unique[wo_users.user_mobile]');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $data['main_content'] = 'create';
        } else {
            $insert = $this->user_model->create_user();
            if ($insert) {
                $data['message'] = "Information Added Successfully.";
                $this->session->set_flashdata('message', 'Information Added Successfully.');
            } else {
                $data['error'] = "Something Went Wrong";
                $this->session->set_flashdata('error', 'Something Went Wrong');
            }
            
        }
        $this->setData($data);
    }

    function changepassword() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('admin/users/signin'));
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cur_password', 'New Password', 'trim|required');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
        $this->form_validation->set_rules('cnf_password', 'Confirm Password', 'trim|required|matches[new_password]');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $user_id = $this->session->userdata('userid');
            $currentPassword = $this->user_model->currentPassword($user_id);
            if (md5($this->input->post('cur_password')) == $currentPassword) {
                $update = $this->user_model->change_password($user_id, $this->input->post('new_password'));
                if ($update) {
                    $this->session->set_flashdata('message', 'Password Changed Successfully.');
                    $data['success'] = 'Password Changed Successfully';
                } else {
                    $data['error'] = 'Not Updated';
                }
            } else {
                $data['error'] = 'Current Password is not same';
            }
        }
        $this->setData($data);
    }
	function admin($id = null) {
		$this->_member_area();
        if ($id != null) {
            $userID = $id;
        } else {
            $userID = $this->session->userdata('userid');
        }
        $includeJs = array(
            'assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
            'assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js',
            'assets/js/core/demo/DemoFormComponents.js'
        );
        $includeCss = array('assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
		
        $data['user'] = $this->user_model->user_by_ids($this->session->userdata('userid'));
        if ($_POST) {
            if ($this->input->post('deleteProfileimage')) {
                $this->user_model->deleteexistingProfileImages($data['user']->user_pic, $this->session->userdata('userid'));
            }
            $userdata = new stdClass();
            $userdata->first_name = $this->input->post('first_name');
            $userdata->last_name = $this->input->post('last_name');
            $userdata->user_mobile = $this->input->post('user_mobile');
            $userdata->user_email = $this->input->post('user_email');
            $userdata->user_mobile = $this->input->post('user_mobile');
            $userdata->user_gender = $this->input->post('user_gender');
            if ($this->input->post('user_password') != '')
            $userdata->user_password = md5($this->input->post('user_password'));
            $insert = $this->user_model->adminUpdate($this->session->userdata('userid'), $userdata);
            if ($insert) {
                $data['message'] = "Updated succesfully";
                $this->session->set_flashdata('message', 'Updated succesfully ');
                $this->setData($data);
				redirect('admin/users/details/1', 'refresh');
				
            }
            return;
        }
        $this->setData($data);
    }

}

?>