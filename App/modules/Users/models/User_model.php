<?php

/*

 */

class User_model extends CI_Model {

    var $table = "wo_users";
    var $users = "users";
    var $usersFeilds = "user_fields";
    var $table_fields_values = "user_fields_values";
    var $original_path;
    var $resized_path;
    var $thumbs_path;

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    function read($showPerpage, $offset) {
        //$this->db->limit($showPerpage, $offset);
        $this->db->order_by('user_id','DESC');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    function usersFeilds($showPerpage, $offset) {
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->usersFeilds);
        $query = $this->db->get();
        return $query->result();
    }

    function count_all() {
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    function user_by_id($id) {
        $this->db->where('user_id', $id);
        $query = $this->db->get($this->table);
        $query->row()->role = $this->get_role($id);
        $query->row()->role_name = $this->get_role_name($query->row()->role);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
	function user_by_ids($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('users');
        $query->row()->role = $this->get_role($id);
        $query->row()->role_name = $this->get_role_name($query->row()->role);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function user_by_nicename($user_nicename) {
        $this->db->where('username', $user_nicename);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $this->user_by_id($query->row()->id);
        } else {
            return false;
        }
    }

    function update($userid, $userdata) {
        $data = (array) $userdata;
        $file = $this->UploadProfilePhoto();
        if ($file['file_name'] != '') {
            $data['user_pic'] = $file['file_name'];
        }
        $where = "user_id = $userid";
        $this->db->set('modification_date', 'NOW()', false);
        $str = $this->db->update_string($this->table, $data, $where);
        $query = $this->db->query($str);
        return $query;
    }
    function adminUpdate($userid, $userdata) {
        $data = (array) $userdata;
        $file = $this->UploadProfilePhoto();
        if ($file['file_name'] != '') {
            $data['user_pic'] = $file['file_name'];
        }
        $where = "id = $userid";
        $this->db->set('modification_date', 'NOW()', false);
        $str = $this->db->update_string($this->users, $data, $where);
        $query = $this->db->query($str);
        return $query;
    }
    public function deleteexistingProfileImage($file, $id) {
        unlink(IMAGESPATH . 'users/profile/' . $file);
        $data['user_pic'] = '';
        $this->db->where('user_id', $id);
        $this->db->update($this->table, $data);
    }
	public function deleteexistingProfileImages($file, $id) {
        unlink(IMAGESPATH . 'users/profile/' . $file);
        $data['user_pic'] = '';
        $this->db->where('id', $id);
        $this->db->update($this->users, $data);
    }
    function get_role($user_id) {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('users_roles');
        if ($query->num_rows() > 0) {
            return (int) $query->row()->role_id;
        } else {
            return 0;
        }
    }

    function get_roles() {
        $query = $this->db->get('roles');
        return $query->result();
    }

    function get_role_name($role_id) {
        $this->db->where('id', $role_id);
        $query = $this->db->get('roles');
        if ($query->num_rows() > 0) {
            return $query->row()->name;
        } else {
            return false;
        }
    }

    function validate($user_email, $password) {
        $this->db->where('user_email', $user_email);
        $this->db->where('user_pass', $password);
        $query = $this->db->get('users');
        if ($query->num_rows() === 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function UploadProfilePhoto() {
        $config = array();
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 6000);
        ini_set('max_execution_time', 6000);
        $config['upload_path'] = IMAGESPATH . 'users/profile/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('user_pic')) {
            $info = $this->upload->data();
            return $info;
        }
    }

    function create_user() {
        $file = $this->UploadProfilePhoto();
        if ($file['file_name'] != '') {
            $data['user_pic'] = $file['file_name'];
        }
        $data['name'] = $this->input->post('name', true);
        $data['user_password'] = md5($this->input->post('user_password', true));
        $data['user_email'] = $this->input->post('user_email', true);
        $data['user_mobile'] = $this->input->post('user_mobile', true);
        $data['country_code'] = $this->input->post('country_code', true);
        $data['user_address'] = $this->input->post('user_address', true);
        $data['is_active'] =  $this->input->post('is_active', true);
        $this->db->set('creation_date', 'NOW()', false);
        $this->db->set('modification_date', 'NOW()', false);
        $query = $this->db->insert($this->table, $data);
        if ($query) {
          
            return true;
        } else {
            return false;
        }
    }

    function update_role($userID, $value) {
        $data = array(
            'user_id' => $userID,
            'role_id' => $value
        );
        $this->db->insert('users_roles', $data);
    }

    function updateRole($userID, $value) {
        $data = array(
            'role_id' => $value
        );
        $this->db->where('user_id', $userID);
        $this->db->update('users_roles', $data);
    }

    function updateStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('user_id', $arr_ids);
                $this->db->delete($this->table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $is_active = "0";
            } else if ($this->input->post('Activate') != null) {
                $is_active = "1";
            }
            $data = array(
                'is_active' => $is_active
            );

            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('user_id', $arr_ids);
                $this->db->update($this->table, $data);
            }
        }
    }

    function reset_password($key) {
        $data = array(
            'user_pass' => md5($this->input->post('new_password')),
        );
        $this->db->where('activation_key', $key);
        $query = $this->db->update($this->table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function getUserKey($email) {
        $this->db->where('user_email', $email);
        $query = $this->db->get($this->table);
        return $query->row()->activation_key;
    }

    function currentPassword($userID) {
        $this->db->where('id', $userID);
        $query = $this->db->get($this->users);
        return $query->row()->user_pass;
    }

    function change_password($user_id, $password) {
        $data = array(
            'user_pass' => md5($password),
        );
        $this->db->where('id', $user_id);
        $query = $this->db->update($this->users, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

}

?>