<div class="card style-transparent">
    <div class="card-body">
        <div class="row">
            <div class = "container">
                <div class="doc-registration-form">
                    <div class="login_wrapper wrapper">
                        <form action="" method="post" name="Login_Form" class="form-signin form floating-label">       
                            <h3 class="form-signin-heading">Please Enter Your email to reset Password : </h3>
                            <hr class="colorgraph"><br>
                            <?php if (@$error): ?>
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($this->session->flashdata('message')) { ?>
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Success ! </strong> <?php echo $this->session->flashdata('message'); ?>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <input type="email" class="form-control" id="user_email" name="user_email" required/>
                                <label for="user_email">Email</label>
                            </div>			
                            <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Send" type="Submit">Send</button>  			
                        </form>			
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
