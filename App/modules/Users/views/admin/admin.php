<style>
#offcanvas-search{display:none;}
</style>

<section>
    <div class="contain-lg">
        <!-- BEGIN BASIC VALIDATION -->
        <div class="row">
            <div class="col-md-12">
                <form class="form form-validate floating-label" enctype="multipart/form-data" novalidate="novalidate" method="post">
                    <div class="card-head style-primary">
                        <div class="tools pull-left">
                            <header>Update  Profile</header>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                             <?php if (@$message): ?>
                                <div class="alert alert-callout alert-success" role="alert">
                                    <strong>Success!</strong> <?php echo $message; ?>
                                </div>
                            <?php endif; ?>
                            <?php if (@$error): ?>
                                <div class="alert alert-callout alert-warning" role="alert">
                                    <strong>Warning!</strong> <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label for="name">First Name</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $user->first_name; ?>" required data-rule-minlength="2">
                            </div>
							 <div class="form-group">
                                <label for="name">Last Name</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $user->last_name; ?>" required data-rule-minlength="2">
                            </div>
                            <div class="form-group">
                                <label for="alias">Email</label>
                                <input type="text" class="form-control" id="user_email" name="user_email"  value="<?php echo $user->user_email; ?>" required >
                            </div>
                            <div class="form-group">
                                <label for="alias">Mobile</label>
                                <input type="text" class="form-control" id="user_mobile" name="user_mobile"  value="<?php echo $user->user_mobile; ?>" required >
                            </div>
                            <div class="form-group">
                                            <div id="img-canvas" class="border-gray height-7" style="    width: 26%;">
                                                <?php if ($user->user_pic != '' && $user->user_pic != null) : ?>
                                                    <img src="<?php echo base_url() . 'uploads/users/profile/' . $user->user_pic; ?>" width="250px">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url(); ?>/assets/img/default-user.jpg" alt="" width="250px"/>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="file" name="user_pic" id="user_pic">
                                                <span  class="checkbox checkbox-styled "><label><input name="deleteProfileimage" type="checkbox" id="deleteProfileimage" value="1" > Delete Current Image</label></span>
                                            </div>
                            </div>
							<div class="form-group">

                                <label for="is_active">Gender</label>
                                <select class="form-control" name="user_gender" id="user_gender">
                                    <option value="">Select</option>
                                    <option value="Male" <?php echo $user->user_gender=='Male' ? 'selected="selected"':'';?>>Male</option>
                                    <option value="Female" <?php echo $user->user_gender=='Female' ? 'selected="selected"':'';?>>Female</option>
                                </select>
                            </div>
                        </div><!--end .card-body -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <button type="submit" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Update<div style="top: 26px; left: 32.5px;" class="ink"></div></button>
                            </div>
                        </div><!--end .card-actionbar -->
                    </div><!--end .card -->
                </form>
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END BASIC VALIDATION -->

    </div><!--end .section-body -->
</section>