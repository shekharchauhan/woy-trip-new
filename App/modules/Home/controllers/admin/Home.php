<?php

class Home extends Admin_Controller {

    function index() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('admin/users/signin'));
        } else {
            $this->admin = $this->session->userdata('logged_in');
        }
        $data['main_content'] = 'index';
        $this->setData($data);
    }

}
?>