<div class="hero-unit">
    <h1>Plabro</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut egestas pulvinar odio nec dignissim. Aenean vulputate, lorem condimentum sollicitudin blandit.</p>
</div>

<div class="row">
    <div class="col-sm-4 col-md-4">
        <h2>Encabezado</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut egestas pulvinar odio nec dignissim. Aenean vulputate, lorem condimentum sollicitudin blandit, leo elit fermentum mauris, eget congue nunc mi vel turpis. Donec consequat, tellus eu pretium semper, nibh lectus blandit eros, quis auctor</p>
    </div>
    <div class="col-sm-4 col-md-4">
        <h2>Encabezado</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut egestas pulvinar odio nec dignissim. Aenean vulputate, lorem condimentum sollicitudin blandit, leo elit fermentum mauris, eget congue nunc mi vel turpis. Donec consequat, tellus eu pretium semper, nibh lectus blandit eros, quis auctor</p>
    </div>
    <div class="col-sm-4 col-md-4">
        <h2>Encabezado</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut egestas pulvinar odio nec dignissim. Aenean vulputate, lorem condimentum sollicitudin blandit, leo elit fermentum mauris, eget congue nunc mi vel turpis. Donec consequat, tellus eu pretium semper, nibh lectus blandit eros, quis auctor</p>
    </div>
</div>





<div class="container">
    <div class="row">
        <div class="col-md-12" data-wow-delay="0.2s">
            <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                <!-- Bottom Carousel Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="<?php echo base_url('theme/images/testimonial.png'); ?>" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="<?php echo base_url('theme/images/testimonial.png'); ?>" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="<?php echo base_url('theme/images/testimonial.png'); ?>" alt="">
                    </li>
                </ol>

                <!-- Carousel Slides / Quotes -->
                <div class="carousel-inner text-center">

                    <!-- Quote 1 -->
                    <div class="item active">
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. !</p>
                                    <small>Someone famous</small>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                    <!-- Quote 2 -->
                    <div class="item">
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                    <small>Someone famous</small>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                    <!-- Quote 3 -->
                    <div class="item">
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. .</p>
                                    <small>Someone famous</small>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                </div>

                <!-- Carousel Buttons Next/Prev -->
                <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>
                <a data-slide="next" href="#quote-carousel" class="right carousel-control"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
            </div>
        </div>
    </div>
</div>
