<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan <ranjan.gaurav@orangemantra.in>
 * Date: 06/10/2016
 * Time: 11:28 AM
 */
class Activity_model extends CI_Model {
	var $activity_table = "wo_activities";
	var $comment_table = "wo_activity_comments";
	var $like_table = "wo_activity_likes";
	var $trip_table = "wo_trips";
	var $media_table = "media";
	var $user_table = "wo_users";
	var $friends_table = "wo_friends";
	var $place_table = "wo_places";
	var $trip_mates = "wo_trip_mates";
	// var $group = "wo_groups";
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'group_model', 'group' );
		date_default_timezone_set ( 'Asia/Kolkata' );
	}
	
	/*
	 * @ Get the list of Activities from wo_activities Table
	 * @ Table: wo_activities
	 * @Author: Gaurav Ranjan
	 */
	public function getActivities($user_id, $start_from) {
		//echo 9080; die();
		$logged_in_user = ( int ) $this->input->post ( 'login_user_id' );
		
		$resource_id = ( int ) $this->input->post ( 'resource_id' );
		
		if ($resource_id != 0) {
			//echo 5576; die();
			$GetGroupMember = $this->GetGroupMemberID ( $resource_id );
			$haystack = array_values ( $GetGroupMember ); // print_r($GetGroupMember); die();
			$find = (in_array ( $logged_in_user, $haystack ));
		}
		$friends = array_column ( $this->getUserFriendsList ( $user_id ), 'friend_id' );
		$loginUserfriends = array_column ( $this->getUserFriendsList ( $logged_in_user ), 'friend_id' );
		$this->db->select ( 'act.activity_id,act.group_id,act.body,act.activity_type,act.place,tt.title as tripname,tt.destination as tripplace,act.resource_id,act.resource_type,act.destination,act.city,act.state,act.like_count,act.comment_count,act.place_id,act.creation_date,EXTRACT(MONTH FROM act.creation_date) as month , EXTRACT(YEAR FROM act.creation_date) as Year,if(med.media_src = "","null",CONCAT("' . base_url ( 'uploads/media' ) . '/",med.media_src)) as media_src,IF(us.user_pic = "","null",CONCAT("' . base_url ( 'uploads/users/profile' ) . '/",us.user_pic)) as user_pic,us.name,us.user_id,us.user_mobile,act.destination as place_name,(select count(*) from wo_activity_likes where poster_id=' . $logged_in_user . ' and resource_id=act.activity_id) as is_liked,if((select count(*) from wo_activity_likes where poster_id=' . $logged_in_user . ' and resource_id=act.activity_id) > 0,(select like_id from wo_activity_likes where poster_id=' . $logged_in_user . ' and resource_id=act.activity_id),"null") as like_id' );
		$activity_id = ( int ) $this->input->post ( 'activity_id' );
		
		if ($activity_id == 0) {
			if ($resource_id == 0) {
				
				// echo 123; die();
				if ($user_id != 0)
					$this->db->where ( 'act.user_id', $user_id );
				if ($logged_in_user != $user_id) {
					$this->db->where ( 'activity_seen', 'public' );
				} else {
					if (! empty ( $loginUserfriends )) {
						$this->db->or_where_in ( 'act.user_id', $loginUserfriends );
						$this->db->where ( 'activity_seen', 'public' );
						// $this->db->where ( 'activity_seen', 'public' );
					}
				}
			} else {
				//echo 6676; die();
				$trip_id = $this->input->post ( 'resource_id' );
				$user_id = $this->input->post ( 'login_user_id' );
				$data ['status'] = 1;
				$this->db->where ( 'trip_id', $trip_id );
				$this->db->where ( 'user_id', $user_id );
				$this->db->update ( 'wo_activities_views', $data );
				
				$this->db->where ( 'act.resource_id', $resource_id );
				if ($logged_in_user != $user_id) {
					
					if ($find == 1) {
						// $this->db->where ( 'act.user_id', $user_id );
						// $this->db->where ( 'activity_seen', 'private' );
						// $this->db->where ( 'activity_type', 'wall' );
						$this->db->where_in ( 'act.user_id', $GetGroupMember );
					} 

					else {
						$this->db->where ( 'act.user_id', $user_id );
						// $this->db->where ( 'activity_seen', 'private' );
						$this->db->where ( 'activity_type', 'wall' );
						// $this->db->where_in ( 'act.user_id', '' );
					}
				} else {
					
					 //echo 990; die();
					if (! empty ( $friends )) {
						$this->db->where ( 'activity_seen', 'public' );
						$this->db->where ( 'act.user_id', $logged_in_user );
						$this->db->or_where_in ( 'act.user_id', $friends );
					}
				}
			}
		} else {
			$this->db->where ( 'activity_id', $activity_id );
		}
		
		$this->db->limit ( 10, $start_from );
		$this->db->order_by ( 'act.modification_date', 'DESC' );
		if ($resource_id != '') {
			$this->db->where ( 'act.resource_id', $resource_id );
		}
		
		$this->db->distinct ();
		$this->db->from ( $this->activity_table . ' act' );
		$this->db->join ( $this->media_table . ' med', 'med.media_id=act.media_id', 'left' );
		// $this->db->join ( 'media_gallery' . ' mg', 'mg.activity_id=act.activity_id', 'inn' );
		$this->db->join ( $this->user_table . ' us', 'us.user_id=act.user_id' );
		$this->db->join ( $this->place_table . ' pl', 'pl.place_id=act.place_id', 'left' );
		$this->db->join ( $this->trip_table . ' tt', 'tt.trip_id = act.resource_id and resource_type="trip"', "left" );
		$this->db->join ( $this->comment_table . ' cc', 'cc.resource_id = act.activity_id', 'left' );
		$res = $this->db->get (); //echo $this->db->last_query(); die();
		/*
		 * if($this->input->post ( 'resource_id' ))
		 * {
		 *
		 *
		 * }
		 */
		// echo $this->db->last_query(); die();
		$arr = array ();
		foreach ( $res->result_array () as $val ) {
			if ($val ['resource_id'] != 0 && $val ['activity_type'] == 'wall') {
				
				$group = explode ( ",", $val ['group_id'] );
				$name = $this->GetGroupsName ( $group ); // print_r($group);
				if ($name != 0) {
					$val ['groupsName'] = [ ];
				}
				
				if ($val ['body']) {
					
					if ($val ['tripname'] == '') {
						$val ['tripTitle'] = "Trip to " . $val ['tripplace'];
					} else {
						$val ['tripTitle'] = $val ['tripname'];
					}
				} else {
					if ($val ['tripname'] == '') {
						$val ['tripTitle'] = "Trip to " . $val ['tripplace'];
					} else {
						$val ['tripTitle'] = "Trip to " . $val ['destination'];
					}
				}
			}
			
			if ($val ['resource_id'] == 0 && $val ['activity_type'] == 'wall') {
				// echo 345;
				$group = explode ( ",", $val ['group_id'] );
				$name = $this->GetGroupsName ( $group );
				if ($name != 0) {
					$val ['groupsName'] = $name;
				} else {
					$val ['groupsName'] = [ ];
				}
			}
			if ($logged_in_user == $val ['user_id']) {
				
				if ($val ['body'] == '') { // Trip Name should be optional and if trip name is not given then Default Trip name as � Trip to [Destination] [ Month] [Year ]� should be generated.
					$monthNum = $val ['month'];
					$dateObj = DateTime::createFromFormat ( '!m', $monthNum );
					$monthName = $dateObj->format ( 'F' );
					if ($val ['activity_type'] == 'wall') {
						$val ['body'] = 'You have shared photo in' . ' ' . $val ['destination']; // . ' ' . $val ['destination'] . ' trip' . ' ' . $monthName . ' ' . $val ['Year'];
					} 

					else {
						$val ['body'] = 'You have created a trip to' . ' ' . "'" . $val ['destination'] . "'";
					}
				} else {
					if ($val ['activity_type'] == 'wall') {
						$val ['body'] = $val ['body']; // . ' ' . $val ['destination'] . ' trip' . ' ' . $monthName . ' ' . $val ['Year'];
					} 

					else {
						$val ['body'] = 'You have created a trip to' . ' ' . "'" . $val ['body'] . "'";
					}
					// $val ['body'] = $val ['body'];
				}
			} else {
				
				// echo 123; die();
				if ($val ['body'] == '') {
					
					$monthNum = $val ['month'];
					$dateObj = DateTime::createFromFormat ( '!m', $monthNum );
					$monthName = $dateObj->format ( 'F' );
					if ($val ['activity_type'] == 'wall') {
						$val ['body'] = $val ['name'] . ' ' . 'has shared photo in' . ' ' . $val ['destination']; // . ' ' . $val ['destination'] . ' trip' . ' ' . $monthName . ' ' . $val ['Year'];
					} 

					else {
						$val ['body'] = $val ['name'] . ' ' . ' has created a trip to' . ' ' . "'" . $val ['destination'] . "'";
					}
				} else {
					if ($val ['activity_type'] == 'wall') {
						$val ['body'] = $val ['body']; // . ' ' . $val ['destination'] . ' trip' . ' ' . $monthName . ' ' . $val ['Year'];
					} 

					else {
						$val ['body'] = $val ['name'] . ' ' . ' has created a trip to' . ' ' . "'" . $val ['body'] . "'";
					}
				}
			}
			$med = $this->GetMedia ( $val ['activity_id'] );
			$val ['gallery'] = $med;
			
			if ($val ['destination'] == "") {
				$val ['place_name'] = $val ['place'];
			}
			
			unset ( $val ['month'] );
			unset ( $val ['Year'] );
			// unset ( $val['resource_id']);
			unset ( $val ['group_id'] );
			unset ( $val ['tripplace'] );
			unset ( $val ['tripname'] );
			array_push ( $arr, $val );
		}
		
		// print_r ( $arr );
		// die ();
		// echo $this->db->last_query(); die();
		
		return $arr;
	}
	public function GetMedia($tripID) {
		error_reporting ( 0 );
		
		$this->db->where ( 'wo_activities.activity_id', $tripID );
		$this->db->select ( 'CONCAT("' . base_url ( 'uploads/media' ) . '/",media_gallery.media_src) as media_src,media_gallery.location' );
		$this->db->from ( $this->activity_table );
		$this->db->join ( 'media_gallery', 'wo_activities.activity_id = media_gallery.activity_id', 'inner' );
		$res = $this->db->get (); // echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function GetGroupMemberID($resource_id) {
		$this->db->distinct ();
		$this->db->where ( 'wo_trips.trip_id', $resource_id );
		$this->db->select ( 'wo_group_members.user_id,' );
		$this->db->from ( 'wo_trips' );
		$this->db->join ( 'wo_group_members', 'wo_trips.group_id = wo_group_members.group_id' );
		$res = $this->db->get (); // echo $this->db->last_query(); die();
		$array = '';
		foreach ( $res->result () as $row ) {
			$array [] = $row->user_id;
		}
		
		return $array;
		// print_r($array);die();
	}
	public function GetGroupsName($resource_id) {
		// $this->db->distinct ();
		$this->db->where_in ( 'wo_groups.group_id', $resource_id );
		$this->db->select ( 'wo_groups.title' );
		$this->db->from ( 'wo_groups' );
		$res = $this->db->get (); // echo $this->db->last_query(); //die();
		$result = $res->result_array ();
		if ($result) {
			return $result;
		} 

		else {
			return 0;
		}
	}
	
	/*
	 * @ To get the friend list of users by phone numbers
	 * @ Table: wo_friends
	 * @ param : user_id
	 */
	public function getUserFriendsList($userID) {
		$query = $this->db->query ( '
        SELECT wo_friends.friend_id
        FROM `wo_friends`
        WHERE `user_id` = "' . $userID . '"
        UNION
        SELECT wo_friends.user_id as  friend_id
        FROM `wo_friends`
        WHERE `friend_id` = "' . $userID . '"' );
		// echo $this->db->last_query(); die();
		$result = $query->result_array (); // print_r($result); die();
		return $result;
	}
	public function getAllGroupMembers($GroupIds) {
		$this->db->distinct ();
		$this->db->where_in ( 'wo_groups.group_id', $GroupIds );
		$this->db->select ( 'wo_users.user_id' );
		$this->db->join ( 'wo_group_members', 'wo_group_members.group_id = wo_groups.group_id' );
		$this->db->join ( 'wo_users', 'wo_group_members.user_id = wo_users.user_id' );
		$result = $this->db->get ( 'wo_groups' ); // echo $this->db->last_query(); die();
		return $result->result_array ();
	}
	
	/*
	 * @ Comment Listing
	 * @ Table: wo_scitity_comments
	 * @ This to Send the Top last addedd comment with an Activity
	 */
	public function getActivityComments() {
		$ComIds = $this->db->select ( 'MAX(comment_id) as comment_id' )->group_by ( 'resource_id' )->from ( $this->comment_table )->get ()->result_array ();
		if (! empty ( $ComIds )) {
			$maxCommentIds = array_column ( $ComIds, 'comment_id' );
			$this->db->select ( 'com.*,if(us.user_pic="", "null", CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",us.user_pic )) user_pic,us.name as user_name,us.user_mobile,us.user_id' );
			$this->db->where_in ( 'comment_id', $maxCommentIds );
			$this->db->from ( $this->comment_table . ' com' );
			$this->db->join ( $this->user_table . ' us', 'us.user_id=com.poster_id' );
			$res = $this->db->get ();
			$comArray = array ();
			foreach ( $res->result_array () as $list ) {
				$ActID = $list ['resource_id'];
				$comArray [$ActID] = $list;
			}
			return ( object ) $comArray;
		} else {
			$comArray = array ();
			return ( object ) $comArray;
		}
	}
	
	/*
	 * @ Comment Listing
	 * @ Table: wo_scitity_comments
	 * @ This to Send the all comment lIsting with an Single Activity
	 */
	public function getSingleActivityComment($resource_id) {
		$this->db->select ( 'com.*,if(us.user_pic="", "null", CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",us.user_pic ) ) user_pic,us.name as user_name,us.user_mobile,us.user_id' );
		$this->db->where ( 'resource_id', $resource_id );
		$this->db->from ( $this->comment_table . ' com' );
		$this->db->join ( $this->user_table . ' us', 'us.user_id=com.poster_id' );
		$res = $this->db->get ();
		return $res->result ();
	}
	
	/*
	 * @ Comment Like an Activity
	 * @ Table: wo_activity_likes
	 * @ Like an activity based upon the activity_id
	 * @ params: resource_id (this is activity_id)
	 */
	public function addLike($data) {
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'resource_id',
							'label' => 'Please Send Resource',
							'rules' => 'trim|required' 
					) 
			);
			
			$this->form_validation->set_rules ( $config );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				$counttotal = $this->countTotalLikes ( $data ['poster_id'], $data ['resource_id'] );
				$totalActivityLikes = $this->countTotalActivityLikes ( $data ['resource_id'] );
				if ($counttotal > 0) {
					$message = array (
							'status' => false,
							'response_code' => '0',
							'messsage' => 'Already liked',
							'total' => $totalActivityLikes 
					);
				} else {
					$likeData = array (
							'resource_id' => $data ['resource_id'],
							'poster_id' => $data ['poster_id'] 
					);
					$query = $this->db->insert ( $this->like_table, $likeData );
					$likeId = $this->db->insert_id ();
					
					if ($query) {
						$message = array (
								'status' => true,
								'response_code' => '1',
								'like_id' => $likeId 
						);
					} else {
						$message = array (
								'status' => false,
								'response_code' => '0' 
						);
					}
				}
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	
	/*
	 * @ Delete the LIke for an activity
	 * @ params: like_id
	 * @ Table: wo_activity_likes
	 */
	public function removeLike($data) {
		$check = $this->db->select ( '*' )->where ( 'like_id', $data ['like_id'] )->from ( $this->like_table )->get ()->num_rows (); // echo $this->db->last_query(); die();
		if ($check > 0) {
			$this->db->where ( 'like_id', $data ['like_id'] );
			$res = $this->db->delete ( $this->like_table );
			if ($res) {
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => 'Unliked Successfully' 
				);
			} else {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'Something went wrong while updating' 
				);
			}
		} else {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => 'Already Unliked' 
			);
		}
		
		return $message;
	}
	
	/*
	 * @ To Count the total Likes of activity based on Activity_id and Poster_id
	 * @ Params: poster_id , resource_id
	 * @ Table: wo_activity_likes
	 */
	public function countTotalLikes($userID, $resId) {
		$this->db->where ( 'poster_id', $userID );
		$this->db->where ( 'resource_id', $resId );
		$this->db->from ( $this->like_table );
		$res = $this->db->get ();
		return $res->num_rows ();
	}
	
	/*
	 * @ To Count the total Likes of activity based on Activity_id.
	 * @ Params: activity_id
	 * @ Table: wo_activities
	 */
	public function countTotalActivityLikes($activity_id) {
		$this->db->select ( 'like_count' );
		$this->db->where ( 'activity_id', $activity_id );
		$this->db->from ( $this->activity_table );
		$res = $this->db->get ();
		return $res->row ()->like_count;
	}
	
	/*
	 * @ Increment the like Count If a new like is made on activity
	 * @ Params: resource_id
	 * @ Table: wo_activities
	 */
	public function incrementLike($data) {
		$condition = array (
				'activity_id' => $data ['resource_id'] 
		);
		$resourceInfo = $this->db->select ( 'like_count,user_id' )->get_where ( $this->activity_table, $condition )->row ();
		$likes = $resourceInfo->like_count + 1;
		$Updatecount = array (
				'like_count' => $likes 
		);
		$updateLikes = $this->db->where ( $condition )->update ( $this->activity_table, $Updatecount );
		if ($updateLikes) {
			$message = array (
					'status' => true,
					'user_id' => $resourceInfo->user_id,
					'total_likes' => $likes 
			);
		} else {
			$message = array (
					'status' => false 
			);
		}
		return $message;
	}
	
	/*
	 * @ Decrement the like Count If a like is deleted or Unlike
	 * @ Params: resource_id
	 * @ Table: wo_activities
	 */
	public function decrementLike($data) {
		$condition = array (
				'activity_id' => $data ['resource_id'] 
		);
		$totalLike = $this->db->select ( 'like_count' )->get_where ( $this->activity_table, $condition )->row ()->like_count;
		$likes = $totalLike - 1;
		$Updatecount = array (
				'like_count' => $likes 
		);
		$updateLikes = $this->db->where ( $condition )->update ( $this->activity_table, $Updatecount );
		if ($updateLikes) {
			return $likes;
		} else {
			return false;
		}
	}
	
	/*
	 * @ Delete An Activity
	 * @ Params: activity_id
	 * @ Table: wo_activities
	 */
	public function delete($id) {
		if (isset ( $id ) && $id != '') {
			$this->db->where ( 'activity_id', $id );
			$query = $this->db->delete ( $this->activity_table );
			if ($query) {
				/*
				 * if Successfullt deleted
				 * then also delete the
				 * Comments and Likes of this activity
				 */
				$this->dropActivityComments ( $id );
				$this->dropActivityLikes ( $id );
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/*
	 * @ Delete The Comments if Activity is Deleted
	 */
	public function dropActivityComments($activityId) {
		$this->db->where ( 'resource_id', $activityId );
		$query = $this->db->delete ( $this->comment_table );
	}
	
	/*
	 * @ Delete The Likes if Activity is Deleted
	 */
	public function dropActivityLikes($activityId) {
		$this->db->where ( 'resource_id', $activityId );
		$query = $this->db->delete ( $this->like_table );
	}
	
	/*
	 * @ Create new Acticvity
	 * @ Parms: user_id,title
	 * @ Table: wo_activity
	 */
	public function createActivity() {
		
		// echo 123;die();
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'user_id',
							'label' => 'User Id',
							'rules' => 'trim|required' 
					) 
			);
			$this->form_validation->set_rules ( $config );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				$latLongs = $this->input->post ( 'destination' ) ? $this->getLatLongInfo ( $this->input->post ( 'destination' ) ) : array (
						'latitude' => '',
						'longitude' => '' 
				);
				// print_r($_FILES['media_src']['name']); die();
				
				if ($this->input->post ( 'resource_id' ) == 0 || $this->input->post ( 'resource_id' ) == null) {
					
					if ($this->input->post ( 'activity_type' ) == 'trip') {
						$trip = $this->createNewTrip ( $latLongs );
						$tripId = ($trip) ? $trip : '0';
					} else {
						$tripId = ($this->input->post ( 'resource_id' )) ? $this->input->post ( 'resource_id' ) : '0';
						$GroupIds = $this->input->post ( 'group_id' );
						$GroupIds = (json_decode ( $GroupIds ));
						$arr = implode ( ",", $GroupIds ); // print_r($GroupIds); die();
					}
				} 

				else {
					$GroupIds = $this->input->post ( 'group_id' );
					$GroupIds = (json_decode ( $GroupIds ));
					$arr = implode ( ",", $GroupIds ); // print_r($GroupIds); die();
				}
				
				$activitydata = array (
						'activity_type' => ($this->input->post ( 'activity_type' )) ? $this->input->post ( 'activity_type' ) : 'wall',
						'resource_type' => ($this->input->post ( 'resource_type' )) ? $this->input->post ( 'resource_type' ) : 'wall',
						'resource_id' => ($tripId) ? $tripId : $this->input->post ( 'resource_id' ),
						'group_id' => $arr,
						'user_id' => ($this->input->post ( 'user_id' )) ? $this->input->post ( 'user_id' ) : '',
						'body' => ($this->input->post ( 'title' )) ? $this->input->post ( 'title' ) : '',
						'destination' => ($this->input->post ( 'destination' )) ? $this->input->post ( 'destination' ) : '',
						'state' => ($this->input->post ( 'state' )) ? $this->input->post ( 'state' ) : '',
						'city' => ($this->input->post ( 'city' )) ? $this->input->post ( 'city' ) : '',
						'place' => ($this->input->post ( 'place' )) ? $this->input->post ( 'place' ) : '',
						'latitude' => $latLongs ['latitude'],
						'longitude' => $latLongs ['longitude'],
						'media_id' => $mediaId,
						'start_date' => ($this->input->post ( 'start_date' )) ? $this->input->post ( 'start_date' ) : '',
						'end_date' => ($this->input->post ( 'end_date' )) ? $this->input->post ( 'end_date' ) : '',
						'activity_seen' => ($this->input->post ( 'activity_seen' )) ? $this->input->post ( 'activity_seen' ) : 'public',
						'creation_date' => date ( 'Y-m-d H:i:s' ),
						'modification_date' => date ( 'Y-m-d H:i:s' ),
						'approval' => '1' 
				); // print_r($arr); die();
				/*
				 * $this->db->set ( 'creation_date', date('Y-m-d H:i:s'), false );
				 * $this->db->set ( 'modification_date', date('Y-m-d H:i:s'), false );
				 */
				$res = $this->db->insert ( $this->activity_table, $activitydata );
				$activity_id = $this->db->insert_id ();
				if ($activity_id) {
					$friends = $this->GetUserFriends ( $this->input->post ( 'user_id' ), $activity_id, $this->input->post ( 'resource_id' ) );
				}
				$path = "uploads/media/"; // Upload directory
				$count = 0;
				// print_r($_FILES['media_src']); //die();
				
				$locations ['location'] = json_decode ( $this->input->post ( 'ImgTag' ) );
				
				// $arr = array();
				foreach ( $locations ['location'] as $i => $loc ) {
					if ($loc === "") {
						$locations ['location'] [$i] = $this->input->post ( 'place' );
					}
				}
				// die();
				// print_r($locations ['location']); die();
				
				$merge = array_merge ( $_FILES ['media_src'], $locations ); // print_r($merge); die();
				$location = '';
				foreach ( $merge ['name'] as $f => $name ) {
					
					// print_r($f);
					
					$location [] = $merge ['location'];
					// print_r($merge['location']);
					if (move_uploaded_file ( $merge ["tmp_name"] [$f], $path . $name ))
						$count ++;
						
						// $file = $this->uploadActivityPic ();
					if ($merge ['name'] != '') {
						$media = $this->createNewMedia ( $name, $activity_id, $f, $merge );
						$mediaId = ($media) ? $media : '0';
					} else {
						$mediaId = '0';
					}
				}
				
				if ($res) {
					
					$message = array (
							'status' => true,
							'response_code' => '1',
							'activity_id' => $activity_id,
							'trip_id' => $tripId,
							'message' => 'Added Successfully' 
					);
				} else {
					$message = array (
							'status' => false,
							'response_code' => '0',
							'message' => 'Database Error Occured' 
					);
				}
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	public function GetUserFriends($userid, $activity_id, $resource_id) {
		$query = $this->db->query ( '
        SELECT wo_friends.friend_id
        FROM `wo_friends`
        WHERE `user_id` = "' . $userid . '"
        UNION
        SELECT wo_friends.user_id as  friend_id
        FROM `wo_friends`
        WHERE `friend_id` = "' . $userid . '"' );
		// echo $this->db->last_query(); die();
		// $result = $query->result_array (); // print_r($result); die();
		// return $result;
		foreach ( $query->result_array () as $res ) {
			$data ['trip_id'] = $resource_id;
			$data ['activity_id'] = $activity_id;
			$data ['user_id'] = $res ['friend_id'];
			$data ['status'] = 0;
			
			$this->db->insert ( 'wo_activities_views', $data );
		}
		// die();
	}
	
	/*
	 * @ Function to get the latitude and longitude from an address
	 * @ Params: address
	 * @ return the latitude and longitude array
	 */
	public function getLatLongInfo($address = '') {
		
		// print_r($address); die();
		$geo = file_get_contents ( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode ( $address ) . '&key=' . GoogleAPIKey );
		$geo = json_decode ( $geo, true ); // print_r($geo); die();
		if ($geo ['status'] = 'OK') {
			$latitude = $geo ['results'] [0] ['geometry'] ['location'] ['lat'];
			$longitude = $geo ['results'] [0] ['geometry'] ['location'] ['lng'];
		} else {
			$latitude = '';
			$longitude = '';
		}
		$details = array (
				'latitude' => $latitude,
				'longitude' => $longitude 
		);
		return $details;
	}
	
	/*
	 * @ Create a new trip
	 * @ Table: wo_trips
	 * @ This function will create an new trip and return and trip id to createActivity function
	 * @ which then create an new activiy of that trip
	 * @ Also create an group of mates for this trip
	 */
	public function createNewTrip($latLongs) {
		if ($this->input->post ( 'group_id' ) == '') {
			$group = $this->group->createGroup (); // echo 777; die();
			$groupId = ($group ['status'] == true) ? $group ['group_id'] : '0';
		} else {
			$groupId = $this->input->post ( 'group_id' );
		}
		
		$tripdata = array (
				'user_id' => ($this->input->post ( 'user_id' )) ? $this->input->post ( 'user_id' ) : '',
				'group_id' => $groupId,
				'title' => ($this->input->post ( 'title' )) ? $this->input->post ( 'title' ) : '',
				'description' => ($this->input->post ( 'description' )) ? $this->input->post ( 'description' ) : '',
				'destination' => ($this->input->post ( 'destination' )) ? $this->input->post ( 'destination' ) : '',
				'latitude' => $latLongs ['latitude'],
				'longitude' => $latLongs ['longitude'],
				'start_date' => ($this->input->post ( 'start_date' )) ? $this->input->post ( 'start_date' ) : 'wall',
				'trip_status' => 'ongoing',
				'creation_date' => date ( 'Y-m-d H:i:s' ),
				'modification_date' => date ( 'Y-m-d H:i:s' ) 
		);
		/*
		 * $this->db->set ( 'creation_date', date('Y-m-d H:i:s'), true );
		 * $this->db->set ( 'modification_date', date('Y-m-d H:i:s'), t ); // print_r($tripdata); die();
		 */
		$query = $this->db->insert ( $this->trip_table, $tripdata ); // echo $this->db->last_query(); die();
		if ($query) {
			
			// echo 000; die();
			$tripId = $this->db->insert_id ();
			
			if ($tripId) {
				
				$mates = $this->addMates ( $tripId );
			}
			
			return $tripId;
		} else {
			return false;
		}
	}
	public function addMates($tripId) {
		$members = json_decode ( $this->input->post ( 'members' ) );
		
		if ($members) {
			array_push ( $members, $this->input->post ( 'user_id' ) );
		}
		
		$NonMembers = $this->input->post ( 'non_members' );
		
		if ($NonMembers) {
			$NonMembers = json_decode ( $this->input->post ( 'non_members' ) ); // print_r($NonMembers); die();
			$AddNewUsers = $this->GetNewusers ( $NonMembers ); // print_r($AddNewUsers); die();
			$NewUsers = array_column ( $AddNewUsers, 'user_id' ); // print_r($NewUsers); //die();
			$members = array_merge ( $members, $NewUsers );
		} 

		else {
			$members = $members;
		}
		
		// print_r($members); die();
		
		if (! empty ( $members )) {
			$IDs = array ();
			$i = 0;
			foreach ( $members as $memberId ) {
				$IDs [$i] ['trip_id'] = $tripId;
				$IDs [$i] ['user_id'] = $memberId;
				
				$i ++;
			}
			
			$this->db->insert_batch ( $this->trip_mates, $IDs );
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * @ Create a new media
	 * @ Table: media
	 * @ This function will create an new media and return and media id to createActivity function
	 * @ Param: user_id , title , description, media_src
	 * @
	 */
	public function createNewMedia($file, $activity_id, $f, $merge) {
		
		// $location = json
		$media = array (
				'media_type' => 'wall',
				'activity_id' => $activity_id,
				'location' => $merge ['location'] [$f],
				'user_id' => $this->input->post ( 'user_id' ),
				'title' => ($this->input->post ( 'title' )) ? $this->input->post ( 'title' ) : '',
				'description' => ($this->input->post ( 'description' )) ? $this->input->post ( 'description' ) : '',
				'media_src' => $file,
				'is_active' => '1' 
		);
		$this->db->set ( 'creation_date', 'NOW()', false );
		$query = $this->db->insert ( 'media_gallery', $media );
		if ($query) {
			$mediaId = $this->db->insert_id ();
			return $mediaId;
		} else {
			return false;
		}
	}
	
	/**
	 * ription : To Upload Acitivity Pic
	 * @ File Name: media_src
	 *
	 * @param
	 *        	$id
	 * @return bool @date : 05/10/2016
	 * @method : Upload Acitivity Pic
	 */
	protected function uploadActivityPic() {
		$config = array ();
		ini_set ( 'upload_max_filesize', '200M' );
		ini_set ( 'post_max_size', '200M' );
		ini_set ( 'max_input_time', 3000 );
		ini_set ( 'max_execution_time', 3000 );
		$config ['upload_path'] = IMAGESPATH . 'media/';
		$config ['allowed_types'] = '*';
		$config ['file_name'] = md5 ( uniqid ( rand (), true ) );
		$this->load->library ( 'upload', $config );
		$this->upload->initialize ( $config );
		if ($this->upload->do_upload ( 'media_src' )) {
			$info = $this->upload->data ();
			return $info;
		}
	}
	public function EndYourTrip($TripID, $UserID) {
		try {
			$this->load->library ( 'form_validation' ); // User_id and trip_id these two feilds are manadatory
			$config = array (
					array (
							'field' => 'user_id',
							'label' => 'user_id',
							'rules' => 'trim|required' 
					),
					array (
							'field' => 'trip_id',
							'label' => 'trip_id',
							'rules' => 'trim|required' 
					) 
			);
			
			$this->form_validation->set_rules ( $config );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				$data = array (
						'trip_status' => 'completed',
						'end_date' => date ( 'Y-m-d H:i:s' ) 
				);
				$this->db->where ( 'trip_id', $TripID );
				$this->db->where ( 'user_id', $UserID );
				$update = $this->db->update ( $this->trip_table, $data ); // Whenever you end your trip , It will update the activity status
				                                                          // ongoing to completed with Current datetime
				if ($update) {
					
					$data = array (
							'activity_status' => 'completed',
							'end_date' => date ( 'Y-m-d H:i:s' ) 
					);
					
					$this->db->where ( 'resource_id', $TripID );
					$this->db->where ( 'user_id', $UserID );
					$update = $this->db->update ( $this->activity_table, $data );
					
					$message = array (
							'status' => true,
							'response_code' => '1',
							// 'group_id' => $insertId,
							'message' => 'Success.. Your trip has been successfully ended' 
					);
				} else {
					$message = array (
							'status' => false,
							'response_code' => '0',
							'message' => 'Database Error Occured' 
					);
				}
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		
		return $message;
	}
	public function AddNewusers($NonMembers) {
		$userID = array ();
		$i = 0;
		foreach ( $NonMembers as $num ) {
			$data ['user_mobile'] = $num;
			$data ['is_active'] = '0';
			$this->db->set ( 'creation_date', 'NOW()', false );
			$this->db->set ( 'modification_date', 'NOW()', false );
			$this->db->insert ( $this->user_table, $data );
			$userID [$i] = $this->db->insert_id ();
			$i ++;
		}
		return $userID;
		// print_r($userID); die();
	}
	public function SendInvitation($NonMembers) {
		$sns = Aws\Sns\SnsClient::factory ( array (
				'version' => '2010-03-31',
				'region' => 'ap-southeast-1',
				'credentials' => array (
						'key' => 'AKIAJAC3UZE2HG244DNQ',
						'secret' => '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' 
				) 
		) );
		$result = $sns->listSubscriptionsByTopic ( array (
				'TopicArn' => 'arn:aws:sns:ap-southeast-1:581428970236:woytrip-stage-reg' 
		) );
		foreach ( $result as $key => $value ) {
			if ($key == 'Subscriptions' && count ( $value )) {
				foreach ( $value as $k => $v ) {
					$result = $sns->unsubscribe ( array (
							// SubscriptionArn is required
							'SubscriptionArn' => $v ['SubscriptionArn'] 
					) );
				}
			}
		}
		
		foreach ( $NonMembers as $mem ) {
			$sns->subscribe ( array (
					// TopicArn is required
					'TopicArn' => 'arn:aws:sns:ap-southeast-1:581428970236:woytrip-stage-reg',
					// Protocol is required
					'Protocol' => 'sms',
					'Endpoint' => '+91' . $mem 
			) );
			$res = $sns->publish ( array (
					'TopicArn' => 'arn:aws:sns:ap-southeast-1:581428970236:woytrip-stage-reg',
					'Message' => "You are invited to join WOY TRIP app." 
			) );
		}
	}
	public function GetNewusers($NonMembers) {
		$this->db->where_in ( 'user_mobile', $NonMembers );
		$this->db->select ( 'user_id' );
		$result = $this->db->get ( 'wo_users' ); // echo $this->db->last_query(); die();
		return $result->result_array ();
	}
}

?>