<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan <ranjan.gaurav@orangemantra.in>
 * Date: 27/10/2016
 * Time: 11:28 AM
 */
class Notification_model extends CI_Model {
	var $table = "wo_notifications";
	var $user_table = "wo_users";
	var $activity_table = "wo_activities";
	function __construct() {
		parent::__construct ();
	}
	
	/*
	 * @ Get the List of unread notification based upon user_id
	 * @ Params: user_id
	 * @ Table: wo_notifications
	 */
	public function getNotifications($user_id) {
		$this->db->select ( 'no.notification_id,no.subject_type,no.params,no.read,no.date as notification_creation_date,us.name as user_name,us.user_mobile,ac.body,ac.activity_id,ac.destination' );
		$this->db->where ( 'no.object_id', $user_id );
		$this->db->where ( 'no.read', '0' );
		$this->db->order_by ( 'no.date', 'DESC' );
		$this->db->from ( $this->table . ' no' );
		$this->db->join ( $this->user_table . ' us', 'us.user_id=no.user_id' );
		$this->db->join ( $this->activity_table . ' ac', 'ac.activity_id=no.subject_id' );
		$res = $this->db->get ();
		return $res->result_array ();
	}
	
	/*
	 * @ Make the notification read
	 * @ Params: notification_id
	 * @ Table: wo_notifications
	 */
	public function changeNotificationStatus($notification_ids = array()) {
		$this->db->where_in ( 'notification_id', $notification_ids );
		$data = array (
				'read' => '1' 
		);
		$update = $this->db->update ( $this->table, $data );
		if ($update) {
			$message = array (
					'status' => true,
					'response_code' => '1',
					'message' => "Successfully read" 
			);
		} else {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => "Database error occured" 
			);
		}
		return $message;
	}
}

?>