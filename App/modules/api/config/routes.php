<?php
/*
 *  Author : Gaurav Ranjan 
 * 
 *  Date   : 10-01-2017
 * 
 *  Description : Routing for All API's
 */



/*
 * Routing for Users
 */
$route ['api/otp'] = "User/otp";
$route ['api/usercreate'] = "User/create";
$route ['api/userupdate'] = "User/update";
$route ['api/friends'] = "User/friends";
$route ['api/profile'] = "User/profile";

/*
 * Routing For Activity
 */
$route ['api/activity'] = 'Activity/index';
$route ['api/activity/create'] = 'Activity/create';
$route ['api/like'] = 'Activity/like';
$route ['api/unlike'] = 'Activity/unlike';
$route ['api/activity/drop'] = "Activity/drop";
$route ['api/endTrip'] = "Activity/endTrip";
/*
 * Routing For Trips
 */
$route ['api/trips'] = 'Trips/index';
$route ['api/getTripMates'] = 'Trips/getTripMates';
$route ['api/deleteTrip'] = 'Trips/deleteTrip';
/*
 * Routing for Comments
 */
$route ['api/comments/add'] = 'Comments/create';
$route ['api/comments/drop'] = 'Comments/drop';
$route ['api/comments'] = 'Comments/list';
/*
 * Routing for Groups
 */
$route ['api/groups'] = 'Groups/list';
$route ['api/groupcreate'] = 'Groups/create';
$route ['api/groupupdate'] = 'Groups/update';
$route ['api/groups/drop'] = 'Groups/drop';
$route ['api/removegroupmember'] = 'Groups/remove_group_member';
/*
 * Routing for Notifications
 */
$route ['api/notification'] = 'Notification/index';
$route ['api/notification/status'] = 'Notification/status';
$route ['api/notification/bulkstatus'] = 'Notification/bulkstatus';

/*
 * Routing for Search
 * 
 */

$route ['api/search'] = 'Search/Search';

?>