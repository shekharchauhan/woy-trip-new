

<section class="style-default-bright">
    <div class="section-header">
        <div class="col-md-12">
            <div class="col-sm-6 col-md-6">
                <h2 class="text-primary">CMS</h2>
            </div>
            <div class="col-sm-6 col-md-6 pull-right">
                <a href="<?php echo base_url('admin/cms/create'); ?>" class="btn btn-primary pull-right">Add New</a>
            </div>
        </div><!--end .col -->
    </div>
    <?php if ($this->session->flashdata('message')) { ?>
        <div class="alert alert-callout alert-success" role="alert">
            <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <div class="section-body">

        <!-- BEGIN DATATABLE 1 -->
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-6 col-md-12">
                    <h4>CMS: Show/hide columns</h4>
                </div>
            </div><!--end .col -->
            <form method="post" name="form1" id="form1" action="">
                <div class="col-lg-12">
                    <div class="table-responsive">

                        <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th><div class="checkbox checkbox-styled"><label><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" ></label></div></th>
                            <th>Title</th>
                            <th>Included In</th>
                            <th>Creation Date</th>
                            <th>Status</th>
                            <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($lists as $list): ?>
                                    <tr class="<?php echo ($list->is_active == '1' ? 'gradeX' : 'gradeA'); ?>">
                                        <td>
                                            <div class="checkbox checkbox-styled"><label><input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $list->id; ?>" ></label></div>
                                        </td>
                                        <td><?php echo $list->title; ?></td>
                                        <td><?php echo ucfirst($list->include_in); ?></td>
                                        <td><?php echo $list->creation_date; ?></td>
                                        <td><?php echo ($list->is_active == '1' ? 'Active' : 'Inactive'); ?></td>
                                        <td><a href="<?php echo base_url('admin/cms/edit') . '/' . $list->id; ?>"><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div><!--end .table-responsive -->
                </div><!--end .col -->

                <div class="col-sm-12 col-md-12">
                    <div class="col-sm-6 col-md-6">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" style="padding:2px">
                                    <input type="submit" name="Activate" value="Activate" class="btn"/>
                                    <input type="submit" name="Deactivate" value="Deactivate" class="btn" />
                                    <input type="submit" name="Delete" value="Delete" class="btn" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-6 col-md-6 pull-right">
                        <?php echo $links; ?>
                    </div>
                </div>
            </form>

        </div><!--end .row -->
        <!-- END DATATABLE 1 -->
        <hr class="ruler-xxl"/>


    </div><!--end .section-body -->
</section>
