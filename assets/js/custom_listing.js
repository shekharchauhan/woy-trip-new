$(document).on("click", ".del_Listing", function () {
    var delID = $(this).attr('name');
    var deleteUrl = $("#P_deleteurl").val();
    bootbox.confirm("Are you sure to delete this ?", function (result) {
        if (result == true) {
            $.ajax({
                type: 'POST',
                url: deleteUrl,
                data: {delID: delID},
                success: function (res) {
                    var obj = JSON.parse(res);
                    if (obj['status'] == true) {
                        location.reload();
                    } else {
                        alert("Bad request");
                    }
                },
            });
        }
    });
});
$(document).ready(function(){
	var imgs;
	var settings = {
		url: base_url+'/admin/activity/uploadImages',
		method: "POST",
		allowedTypes:"jpg,png,gif,doc,pdf,zip",
		fileName: "myfile",
		multiple: true,
		onSuccess:function(files,data,xhr)
		{
			var obj = JSON.parse(data);
			showImage(obj['file']);
		},
		afterUploadAll:function()
		{
			
		},
		onError: function(files,status,errMsg)
		{		
			$("#status").html("<font color='red'>Upload is Failed</font>");
		}
	}
	$("#mulitplefileuploader").uploadFile(settings);
});
function showImage(files){
	$('#status').append('<div class="images_files"><div class="img"><img src="'+base_url+'/uploads/activities/'+files+'" /></div><input type="hidden" value="'+files+'" name="image[]" class="location_image" /><input type="text" name="location[]" class="image_location" /></div>');
}
$(function() {
	var dateToday = new Date();
    $('#birthday1').daterangepicker({
		locale: {
            format: 'MM/DD/YYYY'
        },
		  singleDatePicker: false,
		  minDate: dateToday,
		  calender_style: "picker_4",
		  showDropdowns: true
		}, function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		});
});
function checkall(objForm){
	len = objForm.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++) {
		if (objForm.elements[i].type=='checkbox') {
			objForm.elements[i].checked=objForm.check_all.checked;
		}
	}
}